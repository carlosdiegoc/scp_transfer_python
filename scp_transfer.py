from datetime import datetime
import subprocess
import os
import logging

SERVICE_ACCOUNT = 'osboxes'
PRIVATE_KEY_PATH = '/home/osboxes/.ssh/id_rsa'
SERVER = '34.28.230.10'
LOGFILE = f'/home/osboxes/Documentos/{datetime.strftime(datetime.now(), "%Y-%m-%d")}-file_transfer_feed.log'

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    handlers=[
        logging.StreamHandler(),
        logging.FileHandler(LOGFILE)
    ]
)

def verify_files(source_path: str):
    #TODO TESTAR:
    #* files = [file for file in os.listdir(source_path) if file.endswith('.xls')]
    #* return files or False
    source_path = os.listdir(source_path)
    xls_files = [file for file in source_path if file.endswith('.xls')]
    if(xls_files):
        logging.info("Files to be transfered:")
        for file in xls_files:
            logging.info(file)
        return xls_files
    else:        
        return False

def scp_transfer(source_path: str, remote_path: str):
    command = [
        'scp',
        '-i', PRIVATE_KEY_PATH,
        '-p', source_path,
        f'{SERVICE_ACCOUNT}@{SERVER}:{remote_path}'
    ]

    try:
        subprocess.run(command, check=True)
        logging.info(f"{source_path} transfered successfully!")
        return True

    except subprocess.CalledProcessError as e:
        logging.info("File can not be transferred. Error: %s", str(e))

def move_files(source_path: str, archivation_path: str):
    command = [
        'mv',
        '-v', source_path, archivation_path
    ]
    #TODO TESTAR:
    #* try:
    #*     os.rename(source_path, archivation_path)
    #* except OSError as e:
    #*     logging.info("File can not be moved. Error: %s", str(e))
    try:
        subprocess.run(command, check=True)
        logging.info("File moved successfully!")

    except subprocess.CalledProcessError as e:
        logging.info("File can not be moved. Error: %s", str(e))

def transfer_files(source_path: str, remote_path: str, archivation_path: str):
    #TODO TESTAR: 
    #* Posso transferir os arquivos utilizando apenas verify_files(), sem ler o arquivo de log de transmissão e sem pedir o caminho desse log ao usuário?
    exist_files = verify_files(source_path)
    log_transmission_file = read_log_transmission_file()
    log_transmission_path = log_transmission_file[0]  
    file_names = log_transmission_file[1]   
    if exist_files:
        logging.info(f'Starting to transfer files from {source_path} to {remote_path}.')
        for file in file_names:
            #TODO TESTAR:
            #* complete_file_path = os.path.join(source_path, file)
            complete_file_path = f'{source_path}/{file}'
            if scp_transfer(complete_file_path, remote_path):
                logging.info('All files have been successfully transfered!')
                logging.info(f'Starting to move files from {source_path} to {archivation_path}.')
                move_files(complete_file_path, archivation_path)
        
        #TODO TESTAR:
        #* Caso seja possível desconsiderar a necessidade de ler o arquivo de log para transferir os arquivos, bem como de pedir ao usuário o caminho do arquivo de log:
        # log_transmission_path = input("Enter log transmission file path: ")
        # try:
        #     os.rename(log_transmission_path, os.path.join(archivation_path, os.path.basename(log_transmission_path)))
        #     logging.info("Log transmission file moved successfully!")
        # except OSError as e:
        #     logging.info("Log transmission file can not be moved. Error: %s", str(e))

        command = [
            'mv',
            '-v', log_transmission_path, archivation_path
        ]
        try:
            subprocess.run(command, check=True)
            logging.info("Log transmission file moved successfully!")
        except subprocess.CalledProcessError as e:
            logging.info("Log transmission file can not be moved.", str(e))
    else:
         logging.info('There is no file to be transfered.')

#TODO TESTAR:
#* Possivelmente posso remover essa função do programa.
def read_log_transmission_file():
    logfilepath = input("Enter log transmission file path: ")
    command444 = [
        'chmod', '444', logfilepath
    ]
    try:
        subprocess.run(command444, check=True)
        logging.info('Log transmission file permission changed to 444.')
        with open(logfilepath, 'r') as file:
            lines = file.readlines()
            file_names = [line.strip() for line in lines]
    except subprocess.CalledProcessError as e:
            logging.info("Error when trying to change file permissions, is the path correct?", str(e))

    command644 = [
        'chmod', '444', logfilepath
    ]
    try:
        subprocess.run(command644, check=True)
        logging.info('Log transmission file permission changed to 644.')
    except subprocess.CalledProcessError as e:
            logging.info("Error when trying to change file permissions.", str(e))
    return logfilepath, file_names
   
def start_transfer():  
    options = {
        1: ('/home/osboxes/app01', '/home/osboxes/archive_app01', '/home/osboxes/app01'),
        2: ('/home/osboxes/app02', '/home/osboxes/archive_app02', '/home/osboxes/app02'),
        3: ('/home/osboxes/app03', '/home/osboxes/archive_app03', '/home/osboxes/app03')
    }

    print('''Starting the transfer process... Choose an option below:
        For application Clients, choose 1.
        For application Trade, choose 2.
        For application Position, choose 3.
        To cancel, choose 0.''')

    application = int(input())
    if application == 0:
        print('Process canceled by user.')
        return
    elif application not in options:
        print('You have chosen an invalid option, restart the process.')
        return

    source_path, archivation_path, remote_path = options[application]
    transfer_files(source_path, remote_path, archivation_path)

start_transfer()




